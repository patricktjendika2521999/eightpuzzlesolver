﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EightPuzzleSolver
{
    public partial class Form1 : Form
    {
        delegate void updateCurrBlockCallback();
        public Form1()
        {
            InitializeComponent();

            //State root = new State(0);
            //State a = new State(1);
            //State b = new State(2);
            //State c = new State(3);
            //a.parent =(State)root.Clone();
            //b.parent = (State)a.Clone();
            //c.parent = (State)b.Clone();

            //Console.WriteLine(c.h);
            //Console.WriteLine(c.parent.h);
            //Console.WriteLine(c.parent.parent.h);
            //Console.WriteLine(c.parent.parent.parent.h);
            //if (c.parent.parent.parent == root) {

            //        Console.WriteLine("obj sama");

            //}else
            //{
            //    Console.WriteLine("beda obj");
            //    if(c.parent.parent.parent.h == root.h)
            //    {
            //        Console.WriteLine("CLAP!");
            //    }
            //}

            //pqopen.Enqueue(new State(3));
            //pqopen.Enqueue(new State(2));
            //pqopen.Enqueue(new State(1));

            //foreach (State i in pqopen)
            //{
            //    Console.WriteLine(i.h);
            //}
            //int count1 = pqopen.Count;
            //for (int i = 0; i < count1; i++)
            //{
            //    Console.WriteLine(pqopen.Dequeue().h);
            //}
            //Console.WriteLine("Count2:" + temp.Count);
            //for (int i = 0; i < 3; i++)
            //{
            //    Console.WriteLine(temp.Dequeue().h);
            //}
        }
        public Button[,] blockS = new Button[3,3];
        public Button[,] blockG = new Button[3,3];
        State currState;
        State goalState;
        State stateX;
        Stack<State> open = new Stack<State>();
        Stack<State> closed = new Stack<State>();
        List<State> currpath = new List<State>();
        List<State> pqopen = new List<State>();

        //PriorityQueue<State> pqopen = new PriorityQueue<State>();

        int currlvl = 0;
        Queue qopen = new Queue();
        int ptrPrev = 0;

        private void btnPrev_Click(object sender, EventArgs e)
        {
            if (history.Count > 1 && ptrPrev>0)
            {
                ptrPrev--;
                stateX = (State)history[ptrPrev-1].Clone();
                updateCurrBlock();
                
                if (ptrPrev < 2)
                {
                    btnPrev.Enabled = false;
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ptrPrev = 0;
            history.Clear();
            gbMethod.Enabled = true;
            btnFinish.Enabled = false;
            nmrInput.Enabled = true;
            btnStart.Enabled = false;
            btnAuto.Enabled = false;
            btnStop.Enabled = false;
            btnNext.Enabled = false;
            btnPrev.Enabled = false;
            jumlahstep = -1;
            punyaanak = false;
            backward = false;
            listParent1.Clear();
            listParent2.Clear();
            backPath.Clear();
            returnPath.Clear();
            finished = false;
            mode = "";
            auto = false;
            qopen.Clear();
            currlvl = 0;
            pqopen.Clear();
            currpath.Clear();
            closed.Clear();
            open.Clear();
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    blockS[i, j].Enabled = true;
                    blockS[i, j].Text = "";
                    blockG[i, j].Enabled = true;
                    blockG[i, j].Text = "";
                }
            }
        }

        void setupBlock() {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    blockS[i, j] = new Button();
                    blockS[i, j].Text = btnSample.Text;
                    blockS[i, j].ForeColor = btnSample.ForeColor;
                    blockS[i, j].Size = btnSample.Size;
                    blockS[i, j].BackColor = btnSample.BackColor;
                    blockS[i, j].Left = btnSample.Left + (j * (btnSample.Width));
                    blockS[i, j].Top = btnSample.Top + (i * (btnSample.Height));
                    blockS[i, j].Click += new System.EventHandler(this.btnBlockClick);
                    this.Controls.Add(blockS[i, j]);
                }
            }
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    blockG[i, j] = new Button();
                    blockG[i, j].Text = btnSample2.Text;
                    blockG[i, j].ForeColor = btnSample2.ForeColor;
                    blockG[i, j].Size = btnSample2.Size;
                    blockG[i, j].BackColor = btnSample2.BackColor;
                    blockG[i, j].Left = btnSample2.Left + (j * (btnSample2.Width));
                    blockG[i, j].Top = btnSample2.Top + (i * (btnSample2.Height));
                    blockG[i, j].Click += new System.EventHandler(this.btnBlockClick);
                    this.Controls.Add(blockG[i, j]);
                }
            }
        }

        private void btnBlockClick(object sender, EventArgs e)
        {
            var btn = (Button)sender;
            btn.Text = nmrInput.Value.ToString();
            int ctr1 = 0;
            int ctr2 = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (blockS[i, j].Text != "")
                    {
                        ctr1++;
                    }
                    if (blockG[i, j].Text != "")
                    {
                        ctr2++;
                    }
                }
            }
            if (ctr1 == 8 && ctr2==8)
            {
                btnFinish.Enabled = true;
            }else
            {
                btnFinish.Enabled = false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            setupBlock();
            btnPrev.Enabled = false;
            btnNext.Enabled = false;
            btnStart.Enabled = false;
            btnFinish.Enabled = false;
            btnAuto.Enabled = false;
            btnStop.Enabled = false;
            btnReset.Enabled = true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (ptrPrev != history.Count)
            {
                Console.WriteLine("returnPrev!");
                Console.WriteLine("ptrPrev: "+ptrPrev);
                stateX = (State)history[ptrPrev].Clone();
                updateCurrBlock();
                ptrPrev++;
                btnPrev.Enabled = true;
            }else
            {
                btnPrev.Enabled = true;
                if (mode == "DFS")
                {
                    dfs();
                }
                else if (mode == "BFS")
                {
                    bfs();
                }
                else if (mode == "AStar")
                {
                    astar();
                }
            }
        }
        bool auto = false;
        
        private void btnAuto_Click(object sender, EventArgs e)
        {
            auto = true;
            btnStop.Enabled = true;
            btnNext.Enabled = false;
            btnPrev.Enabled = false;
            btnAuto.Enabled = false;
            tmrAuto.Start();
        }
        
        private void tmrAuto_Tick(object sender, EventArgs e)
        {
            if (auto){
                if (ptrPrev != history.Count) {
                    Console.WriteLine("returnPrev!");
                    Console.WriteLine("ptrPrev: " + ptrPrev);
                    stateX = (State)history[ptrPrev].Clone();
                    updateCurrBlock();
                    ptrPrev++;
                }
                else
                {
                    if (mode == "DFS")
                    {
                        dfs();
                    }
                    else if (mode == "BFS")
                    {
                        bfs();
                    }
                    else if (mode == "AStar")
                    {
                        astar();
                    }
                }
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            tmrAuto.Stop();
            btnStop.Enabled = false;
            btnPrev.Enabled = true;
            btnNext.Enabled = true;
            btnAuto.Enabled = true;
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    blockS[i, j].Enabled = false;
                    blockG[i, j].Enabled = false;
                }
            }
            currState = new State(
                blockS[0, 0].Text, blockS[0, 1].Text, blockS[0, 2].Text,
                blockS[1, 0].Text,blockS[1, 1].Text,blockS[1, 2].Text,
                blockS[2, 0].Text,blockS[2,1].Text,blockS[2,2].Text);
            goalState = new State(
                blockG[0, 0].Text, blockG[0, 1].Text, blockG[0, 2].Text,
                blockG[1, 0].Text, blockG[1, 1].Text, blockG[1, 2].Text,
                blockG[2, 0].Text, blockG[2, 1].Text, blockG[2, 2].Text);
            

            debugS = (State)currState.Clone();
            btnStart.Enabled = true;
            btnFinish.Enabled = false;
            if (rbDFS.Checked)
            {
                mode = "DFS";
                open.Push((State)currState.Clone());
            }
            else if(rbBFS.Checked)
            {
                mode = "BFS";
                qopen.Enqueue((State)currState.Clone());
            }else if (rbAStar.Checked)
            {
                mode = "AStar";
                //pqopen.Enqueue((State)currState.Clone());
                pqopen.Add((State)currState.Clone());
                pqopen.Sort();
            }
            gbMethod.Enabled = false;
            nmrInput.Enabled = false;
        }
        String mode = "";
        bool finished = false;
        
        void step()
        {
            if (stateX.iblank == 0 && stateX.jblank == 0)
            {
                stateX = (State)currState.Clone();
                exchangeRight(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeBottom(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
            }
            else if (stateX.iblank == 0 && stateX.jblank == 1)
            {
                stateX = (State)currState.Clone();
                exchangeRight(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeBottom(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeLeft(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
            }
            else if (stateX.iblank == 0 && stateX.jblank == 2)
            {
                stateX = (State)currState.Clone();
                exchangeBottom(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeLeft(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
            }
            else if (stateX.iblank == 1 && stateX.jblank == 0)
            {
                stateX = (State)currState.Clone();
                exchangeTop(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeRight(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeBottom(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
            }
            else if (stateX.iblank == 1 && stateX.jblank == 1)
            {
                stateX = (State)currState.Clone();
                exchangeTop(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeRight(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeBottom(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeLeft(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
            }
            else if (stateX.iblank == 1 && stateX.jblank == 2)
            {
                stateX = (State)currState.Clone();
                exchangeTop(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeBottom(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeLeft(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
            }
            else if (stateX.iblank == 2 && stateX.jblank == 0)
            {
                stateX = (State)currState.Clone();
                exchangeTop(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeRight(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
            }
            else if (stateX.iblank == 2 && stateX.jblank == 1)
            {
                stateX = (State)currState.Clone();
                exchangeTop(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeRight(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeLeft(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
            }
            else if (stateX.iblank == 2 && stateX.jblank == 2)
            {
                stateX = (State)currState.Clone();
                exchangeTop(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
                stateX = (State)currState.Clone();
                exchangeLeft(stateX.iblank, stateX.jblank, ref stateX);
                checkStateX();
            }
        }
        List<State> history = new List<State>();
        void dfs()
        {
            punyaanak = false;
            if (backward)
            {
                punyaanak = true;
                if (open.Count > 0)
                {
                    currlvl--;
                    if (currlvl != open.Peek().lvl-2)
                    {
                        stateX = (State)currpath[currlvl];
                        currpath.RemoveAt(currlvl + 1);
                        history.Add((State)stateX.Clone());
                        ptrPrev++;
                        updateCurrBlock();
                        if (currlvl - 1 == open.Peek().lvl - 2)
                        {
                            currlvl++;
                            backward = false;
                        }
                    }
                    else
                    {
                        backward = false;
                    }
                }else
                {
                    backward = false;
                }
            }else
            {
                if (open.Count > 0)
                {
                    stateX = (State)open.Pop().Clone();
                    currState = (State)stateX.Clone();
                    currpath.Add((State)stateX.Clone());
                    closed.Push((State)stateX.Clone());
                    history.Add((State)stateX.Clone());
                    ptrPrev++;
                    updateCurrBlock();
                    if (checkState())
                    {
                        tmrAuto.Stop();
                        MessageBox.Show("Finished!");
                        finished = true;
                        updateCurrBlock();
                        btnPrev.Enabled = false;
                        btnNext.Enabled = false;
                        btnAuto.Enabled = false;
                        btnStop.Enabled = false;
                        auto = false;
                    }
                    else
                    {
                        step();
                    }
                    if (punyaanak)
                    {
                        currlvl++;
                    }
                }
            }
            if (!punyaanak)
            {
                backward = true;
            }
            if (open.Count==0 && !finished)
            {
                tmrAuto.Stop();
                MessageBox.Show("Failed");
                updateCurrBlock();
                btnPrev.Enabled = false;
                btnNext.Enabled = false;
                btnAuto.Enabled = false;
                btnStop.Enabled = false;
                auto = false;
            }
        }
        List<State> listParent1 = new List<State>();
        List<State> listParent2 = new List<State>();
        List<State> backPath = new List<State>();
        List<State> returnPath = new List<State>();
        void bfs()
        {
            Console.WriteLine("bfs!");
            if (backward)
            {
                if (backPath.Count > 0)
                {
                    Console.WriteLine("Back!");
                    stateX = (State)backPath[0].Clone();
                    backPath.RemoveAt(0);
                    jumlahstep--;
                    Console.WriteLine("Jumlah Step saat back:" + jumlahstep);
                    history.Add((State)stateX.Clone());
                    ptrPrev++;
                    updateCurrBlock();
                    if (backPath.Count == 0)
                    {
                        if (returnPath.Count == 0)
                        {
                            listParent1.Clear();
                            listParent2.Clear();
                            backward = false;
                        }else
                        {
                            Console.WriteLine("Masuk return!");
                        }
                        
                    }
                }else if (returnPath.Count > 0)
                {
                    Console.WriteLine("Returning!");
                    stateX = (State)returnPath[returnPath.Count-1].Clone();
                    returnPath.RemoveAt(returnPath.Count - 1);
                    jumlahstep++;
                    Console.WriteLine("Jumlah Step saat returning:" + jumlahstep);
                    history.Add((State)stateX.Clone());
                    ptrPrev++;
                    updateCurrBlock();
                    if (returnPath.Count == 0 && backPath.Count == 0)
                    {
                        listParent1.Clear();
                        listParent2.Clear();
                        backward = false;
                    }
                }
            }
            else
            {
                if (qopen.Count > 0)
                {
                    jumlahstep++;
                    Console.WriteLine("Jumlah Step normal: "+jumlahstep);
                    Console.WriteLine("qopen.Count: " + qopen.Count);
                    stateX = (State)qopen.Dequeue();
                    currState = (State)stateX.Clone();
                    currpath.Add((State)stateX.Clone());
                    closed.Push((State)stateX.Clone());
                    history.Add((State)stateX.Clone());
                    ptrPrev++;
                    updateCurrBlock();
                    if (checkState())
                    {
                        tmrAuto.Stop();
                        MessageBox.Show("Finished");
                        finished = true;
                        updateCurrBlock();
                        btnPrev.Enabled = false;
                        btnNext.Enabled = false;
                        btnAuto.Enabled = false;
                        btnStop.Enabled = false;
                        auto = false;
                    }
                    else
                    {
                        step();
                        int ctr = 0;
                        State nextS = (State)qopen.Peek();
                        for (int i = 0; i < 3; i++)
                        {
                            for (int j = 0; j < 3; j++)
                            {
                                if (currState.value[i, j] == nextS.parent.value[i, j])
                                {
                                    ctr++;
                                }
                            }
                        }
                        if (ctr != 9)
                        {
                            Console.WriteLine("BW!");
                            backward = true;
                            //stateX = (State)currState.Clone();
                            State ptrBack = (State)currState.Clone();
                            while (ptrBack.parent != null)
                            {
                                listParent1.Add((State)ptrBack.parent.Clone());
                                ptrBack = ptrBack.parent;
                            }
                            ptrBack = (State)nextS.Clone();
                            while (ptrBack.parent != null)
                            {
                                listParent2.Add((State)ptrBack.parent.Clone());
                                ptrBack = ptrBack.parent;
                            }
                            bool found = false;
                            for (int i = 0; i < listParent1.Count; i++)
                            {
                                if (!found)
                                {
                                    backPath.Add(listParent1[i]);
                                    returnPath.Clear();
                                    for (int j = 0; j < listParent2.Count; j++)
                                    {
                                        int ctr2 = 0;
                                        for (int k = 0; k < 3; k++)
                                        {
                                            for (int l = 0; l < 3; l++)
                                            {
                                                if (listParent1[i].value[k, l] == listParent2[j].value[k, l])
                                                {
                                                    ctr2++;
                                                }
                                            }
                                        }
                                        Console.WriteLine("ctr2:" + ctr2);
                                        if (ctr2 == 9)
                                        {
                                            Console.WriteLine("BREAK!");
                                            found = true;
                                            break;
                                        }
                                        Console.WriteLine("Testing");
                                        returnPath.Add(listParent2[j]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (qopen.Count == 0 && !finished)
            {
                tmrAuto.Stop();
                MessageBox.Show("Failed");
                updateCurrBlock();
                btnPrev.Enabled = false;
                btnNext.Enabled = false;
                btnAuto.Enabled = false;
                btnStop.Enabled = false;
                auto = false;
            }
        }

        void astar()
        {
            Console.WriteLine("astar!");
            if (backward)
            {
                if (backPath.Count > 0)
                {
                    Console.WriteLine("Back!");
                    stateX = (State)backPath[0].Clone();
                    backPath.RemoveAt(0);
                    jumlahstep--;
                    Console.WriteLine("Jumlah Step saat back:" + jumlahstep);
                    history.Add((State)stateX.Clone());
                    ptrPrev++;
                    updateCurrBlock();
                    if (backPath.Count == 0)
                    {
                        if (returnPath.Count == 0)
                        {
                            listParent1.Clear();
                            listParent2.Clear();
                            backward = false;
                        }else
                        {
                            Console.WriteLine("Masuk return!");
                        }
                    }
                }
                else if (returnPath.Count > 0)
                {
                    Console.WriteLine("Returning!");
                    stateX = (State)returnPath[returnPath.Count - 1].Clone();
                    returnPath.RemoveAt(returnPath.Count - 1);
                    jumlahstep++;
                    Console.WriteLine("Jumlah Step saat returning:" + jumlahstep);
                    history.Add((State)stateX.Clone());
                    ptrPrev++;
                    updateCurrBlock();
                    if (returnPath.Count == 0 && backPath.Count == 0)
                    {
                        listParent1.Clear();
                        listParent2.Clear();
                        backward = false;
                    }
                }
            }
            else
            {
                if (pqopen.Count > 0)
                {
                    jumlahstep++;
                    Console.WriteLine("Jumlah Step normal: " + jumlahstep);
                    Console.WriteLine("pqopen.Count: " + pqopen.Count);
                    stateX = (State)pqopen[0];
                    pqopen.RemoveAt(0);
                    currState = (State)stateX.Clone();
                    currpath.Add((State)stateX.Clone());
                    closed.Push((State)stateX.Clone());
                    history.Add((State)stateX.Clone());
                    ptrPrev++;
                    updateCurrBlock();
                    if (checkState())
                    {
                        tmrAuto.Stop();
                        MessageBox.Show("Finished");
                        finished = true;
                        updateCurrBlock();
                        btnPrev.Enabled = false;
                        btnNext.Enabled = false;
                        btnAuto.Enabled = false;
                        btnStop.Enabled = false;
                        auto = false;
                    }
                    else
                    {
                        step();
                        int ctr = 0;
                        State nextS = (State)pqopen[0];
                        for (int i = 0; i < 3; i++)
                        {
                            for (int j = 0; j < 3; j++)
                            {
                                if (currState.value[i, j] == nextS.parent.value[i, j])
                                {
                                    ctr++;
                                }
                            }
                        }
                        if (ctr != 9)
                        {
                            Console.WriteLine("BW!");
                            backward = true;
                            State ptrBack = (State)currState.Clone();
                            while (ptrBack.parent != null)
                            {
                                listParent1.Add((State)ptrBack.parent.Clone());
                                ptrBack = ptrBack.parent;
                            }
                            ptrBack = (State)nextS.Clone();
                            while (ptrBack.parent != null)
                            {
                                listParent2.Add((State)ptrBack.parent.Clone());
                                ptrBack = ptrBack.parent;
                            }
                            bool found = false;
                            for (int i = 0; i < listParent1.Count; i++)
                            {
                                if (!found)
                                {
                                    backPath.Add(listParent1[i]);
                                    returnPath.Clear();
                                    for (int j = 0; j < listParent2.Count; j++)
                                    {
                                        int ctr2 = 0;
                                        for (int k = 0; k < 3; k++)
                                        {
                                            for (int l = 0; l < 3; l++)
                                            {
                                                if (listParent1[i].value[k, l] == listParent2[j].value[k, l])
                                                {
                                                    ctr2++;
                                                }
                                            }
                                        }
                                        Console.WriteLine("ctr2:" + ctr2);
                                        if (ctr2 == 9)
                                        {
                                            Console.WriteLine("BREAK!");
                                            found = true;
                                            break;
                                        }
                                        Console.WriteLine("Testing");
                                        returnPath.Add(listParent2[j]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (pqopen.Count == 0 && !finished)
            {
                tmrAuto.Stop();
                MessageBox.Show("Failed");
                updateCurrBlock();
                btnPrev.Enabled = false;
                btnNext.Enabled = false;
                btnAuto.Enabled = false;
                btnStop.Enabled = false;
                auto = false;
            }
        }

        State debugS;
        bool checkState()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (stateX.value[i, j]!=goalState.value[i,j]) {
                        return false;
                    }
                }
            }
            return true;
        }
        void checkStateX()
        {
            bool sama = false;
            int ctr;
            if (mode == "DFS")
            {
                foreach (State o in open)
                {
                    ctr = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            if (stateX.value[i, j] == o.value[i, j])
                            {
                                ctr++;
                            }
                        }
                    }
                    if (ctr == 9)
                    {
                        sama = true;
                    }
                }
            }else if (mode == "BFS")
            {
                foreach (State o in qopen)
                {
                    ctr = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            if (stateX.value[i, j] == o.value[i, j])
                            {
                                ctr++;
                            }
                        }
                    }
                    if (ctr == 9)
                    {
                        sama = true;
                    }
                }
            }else if (mode == "AStar")
            {
                //int count = pqopen.Count;

                foreach (State o in pqopen)
                {
                    ctr = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            if (stateX.value[i, j] == o.value[i, j])
                            {
                                ctr++;
                            }
                        }
                    }
                    if (ctr == 9)
                    {
                        sama = true;
                    }
                }
            }
            
            int ctr2;
            foreach (State c in closed)
            {
                ctr2 = 0;
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (stateX.value[i, j] == c.value[i, j])
                        {
                            ctr2++;
                        }
                    }
                }
                if (ctr2 == 9)
                {
                    sama = true;
                }
            }
            if (!sama)
            {
                if (mode == "DFS")
                {
                    currlvl++;
                    stateX.lvl = currlvl;
                    open.Push((State)stateX.Clone());
                    if (stateX.iblank == 1 && stateX.jblank == 1)
                    {
                        debugS = stateX;
                    }
                    currlvl--;
                    punyaanak = true;
                }else if(mode=="BFS")
                {
                    stateX.parent = (State)currState.Clone();
                    qopen.Enqueue((State)stateX.Clone());
                }
                else if (mode == "AStar")
                {
                    int h = 9;
                    
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            if (stateX.value[i, j] == goalState.value[i, j])
                            {
                                h--;
                            }
                        }
                    }
                    stateX.parent = (State)currState.Clone();
                    
                    stateX.h = h;
                    stateX.g = jumlahstep+1;
                    stateX.f = stateX.h + stateX.g;
                    Console.WriteLine("h:" + stateX.h);
                    Console.WriteLine("g:" + stateX.g);
                    pqopen.Add((State)stateX.Clone());
                    Console.WriteLine("sebelum sort");
                    Console.WriteLine(pqopen[0].f);
                    pqopen.Sort();
                    Console.WriteLine("sesudah sort");
                    Console.WriteLine(pqopen[0].f);
                    //qopen.Enqueue((State)stateX.Clone());
                }
            }
        }
        int jumlahstep = -1;
        bool punyaanak = false;
        bool backward = false;
        void exchangeTop(int iblank, int jblank, ref State stateX)
        {
            swap(iblank, jblank, iblank-1, jblank, ref stateX);
            stateX.iblank -= 1;
        }
        void exchangeRight(int iblank, int jblank, ref State stateX)
        {
            swap(iblank, jblank, iblank, jblank+1, ref stateX);
            stateX.jblank += 1;
        }
        void exchangeBottom(int iblank, int jblank, ref State stateX)
        {
            swap(iblank, jblank, iblank+1, jblank, ref stateX);
            stateX.iblank += 1;
        }
        void exchangeLeft(int iblank, int jblank, ref State stateX)
        {
            swap(iblank, jblank, iblank, jblank-1, ref stateX);
            stateX.jblank -= 1;
        }
        void swap(int ix1, int jx1, int ix2, int jx2, ref State x)
        {
            String temp="";
            temp = x.value[ix1, jx1];
            x.value[ix1, jx1] = x.value[ix2, jx2];
            x.value[ix2, jx2] = temp;
        }
        void updateCurrBlock()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (this.blockS[i, j].InvokeRequired)
                    {
                        updateCurrBlockCallback d = new updateCurrBlockCallback(updateCurrBlock);
                        this.Invoke(d, new object[] {});
                    }else
                    {
                        blockS[i, j].Text = stateX.value[i, j];
                    }
                }
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnNext.Enabled = true;
            btnAuto.Enabled = true;
            btnStart.Enabled = false;
            if (mode == "DFS")
            {
                dfs();
            }else if (mode == "BFS")
            {
                bfs();
            }
            else if (mode == "AStar")
            {
                astar();
            }
        }
    }
}
