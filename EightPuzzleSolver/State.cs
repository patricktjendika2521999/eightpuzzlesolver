﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EightPuzzleSolver
{
    class State : ICloneable, IComparable
    {
        public String[,] value = new String[3, 3];
        public int lvl,h,g,f;
        public State parent;
        //menyimpan value mana yang kosong
        public int iblank,jblank;

        public State(String v00, String v01, String v02, String v10, String v11, String v12, String v20, String v21, String v22)
        {
            value[0, 0] = v00;
            value[0, 1] = v01;
            value[0, 2] = v02;
            value[1, 0] = v10;
            value[1, 1] = v11;
            value[1, 2] = v12;
            value[2, 0] = v20;
            value[2, 1] = v21;
            value[2, 2] = v22;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (value[i, j] == "")
                    {
                        iblank = i;
                        jblank = j;
                    }
                }
            }
        }

        public State(int h)
        {
            this.h = h;
        }

        public bool checkParent()
        {
            return true;
        }

        public object Clone()
        {
            State deepcopyState = new State(this.value[0, 0], this.value[0, 1], this.value[0, 2], this.value[1, 0], this.value[1, 1], this.value[1, 2], this.value[2, 0], this.value[2, 1], this.value[2, 2]);
            deepcopyState.lvl = this.lvl;
            deepcopyState.iblank = this.iblank;
            deepcopyState.jblank = this.jblank;
            deepcopyState.h = this.h;
            deepcopyState.g = this.g;
            deepcopyState.f = this.f;
            deepcopyState.parent = this.parent;
            return deepcopyState;
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;
            State other = obj as State;
            if(other != null)
            {
                return this.f.CompareTo(other.f);
            }
            else
            {
                throw new ArgumentException("Object is not a State");
            }
        }
    }
}
