﻿namespace EightPuzzleSolver
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSample = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.gbMethod = new System.Windows.Forms.GroupBox();
            this.rbAStar = new System.Windows.Forms.RadioButton();
            this.rbBFS = new System.Windows.Forms.RadioButton();
            this.rbDFS = new System.Windows.Forms.RadioButton();
            this.nmrInput = new System.Windows.Forms.NumericUpDown();
            this.btnSample2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnFinish = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnAuto = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.tmrAuto = new System.Windows.Forms.Timer(this.components);
            this.btnReset = new System.Windows.Forms.Button();
            this.gbMethod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmrInput)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(102, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Start";
            // 
            // btnSample
            // 
            this.btnSample.Location = new System.Drawing.Point(77, 131);
            this.btnSample.Name = "btnSample";
            this.btnSample.Size = new System.Drawing.Size(25, 25);
            this.btnSample.TabIndex = 0;
            this.btnSample.UseVisualStyleBackColor = true;
            this.btnSample.Visible = false;
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(149, 234);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 2;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // gbMethod
            // 
            this.gbMethod.Controls.Add(this.rbAStar);
            this.gbMethod.Controls.Add(this.rbBFS);
            this.gbMethod.Controls.Add(this.rbDFS);
            this.gbMethod.Location = new System.Drawing.Point(12, 8);
            this.gbMethod.Name = "gbMethod";
            this.gbMethod.Size = new System.Drawing.Size(155, 53);
            this.gbMethod.TabIndex = 3;
            this.gbMethod.TabStop = false;
            this.gbMethod.Text = "Method";
            // 
            // rbAStar
            // 
            this.rbAStar.AutoSize = true;
            this.rbAStar.Location = new System.Drawing.Point(114, 20);
            this.rbAStar.Name = "rbAStar";
            this.rbAStar.Size = new System.Drawing.Size(36, 17);
            this.rbAStar.TabIndex = 3;
            this.rbAStar.TabStop = true;
            this.rbAStar.Text = "A*";
            this.rbAStar.UseVisualStyleBackColor = true;
            // 
            // rbBFS
            // 
            this.rbBFS.AutoSize = true;
            this.rbBFS.Location = new System.Drawing.Point(63, 20);
            this.rbBFS.Name = "rbBFS";
            this.rbBFS.Size = new System.Drawing.Size(45, 17);
            this.rbBFS.TabIndex = 1;
            this.rbBFS.TabStop = true;
            this.rbBFS.Text = "BFS";
            this.rbBFS.UseVisualStyleBackColor = true;
            // 
            // rbDFS
            // 
            this.rbDFS.AutoSize = true;
            this.rbDFS.Location = new System.Drawing.Point(11, 20);
            this.rbDFS.Name = "rbDFS";
            this.rbDFS.Size = new System.Drawing.Size(46, 17);
            this.rbDFS.TabIndex = 0;
            this.rbDFS.TabStop = true;
            this.rbDFS.Text = "DFS";
            this.rbDFS.UseVisualStyleBackColor = true;
            // 
            // nmrInput
            // 
            this.nmrInput.Location = new System.Drawing.Point(256, 28);
            this.nmrInput.Name = "nmrInput";
            this.nmrInput.Size = new System.Drawing.Size(81, 20);
            this.nmrInput.TabIndex = 4;
            // 
            // btnSample2
            // 
            this.btnSample2.Location = new System.Drawing.Point(187, 131);
            this.btnSample2.Name = "btnSample2";
            this.btnSample2.Size = new System.Drawing.Size(25, 25);
            this.btnSample2.TabIndex = 5;
            this.btnSample2.UseVisualStyleBackColor = true;
            this.btnSample2.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(213, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Goal";
            // 
            // btnPrev
            // 
            this.btnPrev.Location = new System.Drawing.Point(68, 234);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(75, 23);
            this.btnPrev.TabIndex = 7;
            this.btnPrev.Text = "Previous";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnFinish
            // 
            this.btnFinish.Location = new System.Drawing.Point(343, 25);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(75, 23);
            this.btnFinish.TabIndex = 8;
            this.btnFinish.Text = "Finish";
            this.btnFinish.UseVisualStyleBackColor = true;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(343, 54);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 9;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnAuto
            // 
            this.btnAuto.Location = new System.Drawing.Point(230, 216);
            this.btnAuto.Name = "btnAuto";
            this.btnAuto.Size = new System.Drawing.Size(75, 23);
            this.btnAuto.TabIndex = 10;
            this.btnAuto.Text = "Auto";
            this.btnAuto.UseVisualStyleBackColor = true;
            this.btnAuto.Click += new System.EventHandler(this.btnAuto_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(230, 245);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 11;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // tmrAuto
            // 
            this.tmrAuto.Interval = 400;
            this.tmrAuto.Tick += new System.EventHandler(this.tmrAuto_Tick);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(311, 234);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 12;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 321);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnAuto);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnFinish);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSample2);
            this.Controls.Add(this.nmrInput);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSample);
            this.Controls.Add(this.gbMethod);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbMethod.ResumeLayout(false);
            this.gbMethod.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmrInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSample;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.GroupBox gbMethod;
        private System.Windows.Forms.RadioButton rbBFS;
        private System.Windows.Forms.RadioButton rbDFS;
        private System.Windows.Forms.NumericUpDown nmrInput;
        private System.Windows.Forms.Button btnSample2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnFinish;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.RadioButton rbAStar;
        private System.Windows.Forms.Button btnAuto;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Timer tmrAuto;
        private System.Windows.Forms.Button btnReset;
    }
}

